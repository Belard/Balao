﻿namespace Balao
{
    using System;
    using System.Windows.Forms;
    public partial class Form1 : Form
    {
        Balao balao;
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonNovoBalao_Click(object sender, EventArgs e)
        {
            // Ativa botões subir, descer e mudaCor
            ButtonDescer.Enabled = true;
            ButtonSubir.Enabled = true;
            ButtonMudarCor.Enabled = true;

            TextBoxCor.Enabled = true;
            // cria instância
            balao = new Balao("azul");
            LabelCor.Text = balao.GetCorBalao();
            LabelDirecao.Text = balao.GetDirecao();
            LabelAltura.Text = balao.GetAltura().ToString() + " m.";
        }

        private void ButtonSubir_Click(object sender, EventArgs e)
        {
            balao.Sobe(0.8);
            balao.SetDirecao("a subir...");
            LabelAltura.Text = balao.GetAltura().ToString();
            LabelDirecao.Text = balao.GetDirecao();
        }

        private void ButtonDescer_Click(object sender, EventArgs e)
        {
            balao.Desce(0.8);
            balao.SetDirecao("a descer...");
            LabelAltura.Text = balao.GetAltura().ToString();
            LabelDirecao.Text = balao.GetDirecao();
        }

        private void ButtonMudarCor_Click(object sender, EventArgs e)
        {
            if(TextBoxCor.Text.Trim() != "")
            {
                balao.SetCorBalao(TextBoxCor.Text.Trim());
                LabelCor.Text = balao.GetCorBalao();
                TextBoxCor.Text = "";
            }
        }
    }
}
