﻿namespace Balao
{
    class Balao
    {
        #region Atributos

        private string _cor;
        private string _direcao;
        private double _altura;
        //public string Cor { get; set; }
        //public string Direcao { get; set; }
        //public string Altura { get; set; }

        #endregion

        #region Métodos

        // Construtores
        public Balao(string cor)
        {
            _cor = cor;
            _direcao = "parado";
            _altura = 0;
        }

        public string GetCorBalao()
        {
            return _cor;
        }
        public void SetCorBalao(string cor)
        {
            _cor = cor;
        }
        public string GetDirecao()
        {
            return _direcao;
        }
        public void SetDirecao(string direcao)
        {
            _direcao = direcao;
        }
        public double GetAltura()
        {
            return _altura;
        }
        public void Sobe(double x)
        {         
            _altura += x;
        }
        public void Desce(double y)
        {
            if(_altura - y >= 0)
                _altura -= y;
        }
        #endregion
    }
}
