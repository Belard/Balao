﻿namespace Balao
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonNovoBalao = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelCor = new System.Windows.Forms.Label();
            this.LabelDirecao = new System.Windows.Forms.Label();
            this.LabelAltura = new System.Windows.Forms.Label();
            this.ButtonSubir = new System.Windows.Forms.Button();
            this.ButtonDescer = new System.Windows.Forms.Button();
            this.ButtonMudarCor = new System.Windows.Forms.Button();
            this.TextBoxCor = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ButtonNovoBalao
            // 
            this.ButtonNovoBalao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonNovoBalao.Location = new System.Drawing.Point(342, 267);
            this.ButtonNovoBalao.Name = "ButtonNovoBalao";
            this.ButtonNovoBalao.Size = new System.Drawing.Size(120, 35);
            this.ButtonNovoBalao.TabIndex = 0;
            this.ButtonNovoBalao.Text = "Novo Balão";
            this.ButtonNovoBalao.UseVisualStyleBackColor = true;
            this.ButtonNovoBalao.Click += new System.EventHandler(this.ButtonNovoBalao_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(316, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cor:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Direção:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(305, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Altura:";
            // 
            // LabelCor
            // 
            this.LabelCor.AutoSize = true;
            this.LabelCor.Location = new System.Drawing.Point(348, 49);
            this.LabelCor.Name = "LabelCor";
            this.LabelCor.Size = new System.Drawing.Size(22, 13);
            this.LabelCor.TabIndex = 4;
            this.LabelCor.Text = ". . .";
            // 
            // LabelDirecao
            // 
            this.LabelDirecao.AutoSize = true;
            this.LabelDirecao.Location = new System.Drawing.Point(348, 77);
            this.LabelDirecao.Name = "LabelDirecao";
            this.LabelDirecao.Size = new System.Drawing.Size(22, 13);
            this.LabelDirecao.TabIndex = 5;
            this.LabelDirecao.Text = ". . .";
            // 
            // LabelAltura
            // 
            this.LabelAltura.AutoSize = true;
            this.LabelAltura.Location = new System.Drawing.Point(348, 105);
            this.LabelAltura.Name = "LabelAltura";
            this.LabelAltura.Size = new System.Drawing.Size(22, 13);
            this.LabelAltura.TabIndex = 6;
            this.LabelAltura.Text = ". . .";
            // 
            // ButtonSubir
            // 
            this.ButtonSubir.Enabled = false;
            this.ButtonSubir.Location = new System.Drawing.Point(51, 157);
            this.ButtonSubir.Name = "ButtonSubir";
            this.ButtonSubir.Size = new System.Drawing.Size(75, 31);
            this.ButtonSubir.TabIndex = 7;
            this.ButtonSubir.Text = "Subir";
            this.ButtonSubir.UseVisualStyleBackColor = true;
            this.ButtonSubir.Click += new System.EventHandler(this.ButtonSubir_Click);
            // 
            // ButtonDescer
            // 
            this.ButtonDescer.Enabled = false;
            this.ButtonDescer.Location = new System.Drawing.Point(51, 205);
            this.ButtonDescer.Name = "ButtonDescer";
            this.ButtonDescer.Size = new System.Drawing.Size(75, 31);
            this.ButtonDescer.TabIndex = 8;
            this.ButtonDescer.Text = "Descer";
            this.ButtonDescer.UseVisualStyleBackColor = true;
            this.ButtonDescer.Click += new System.EventHandler(this.ButtonDescer_Click);
            // 
            // ButtonMudarCor
            // 
            this.ButtonMudarCor.Enabled = false;
            this.ButtonMudarCor.Location = new System.Drawing.Point(51, 274);
            this.ButtonMudarCor.Name = "ButtonMudarCor";
            this.ButtonMudarCor.Size = new System.Drawing.Size(86, 28);
            this.ButtonMudarCor.TabIndex = 9;
            this.ButtonMudarCor.Text = "Mudar Cor";
            this.ButtonMudarCor.UseVisualStyleBackColor = true;
            this.ButtonMudarCor.Click += new System.EventHandler(this.ButtonMudarCor_Click);
            // 
            // TextBoxCor
            // 
            this.TextBoxCor.Enabled = false;
            this.TextBoxCor.Location = new System.Drawing.Point(152, 279);
            this.TextBoxCor.Name = "TextBoxCor";
            this.TextBoxCor.Size = new System.Drawing.Size(125, 20);
            this.TextBoxCor.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 339);
            this.Controls.Add(this.TextBoxCor);
            this.Controls.Add(this.ButtonMudarCor);
            this.Controls.Add(this.ButtonDescer);
            this.Controls.Add(this.ButtonSubir);
            this.Controls.Add(this.LabelAltura);
            this.Controls.Add(this.LabelDirecao);
            this.Controls.Add(this.LabelCor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonNovoBalao);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonNovoBalao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelCor;
        private System.Windows.Forms.Label LabelDirecao;
        private System.Windows.Forms.Label LabelAltura;
        private System.Windows.Forms.Button ButtonSubir;
        private System.Windows.Forms.Button ButtonDescer;
        private System.Windows.Forms.Button ButtonMudarCor;
        private System.Windows.Forms.TextBox TextBoxCor;
    }
}

